package users

import "time"

type User struct {
	ID         string    `json:"id"`
	Pid        string    `json:"pid"`
	Title      string    `json:"title"`
	FirstName  string    `json:"first_name"`
	LastName   string    `json:"last_name"`
	NickName   string    `json:"nick_name"`
	BirthDate  time.Time `json:"birth_date"`
	Status     string    `json:"status"`
	JoinDate   time.Time `json:"join_date"`
	LastUpdate time.Time `json:"last_update"`
}
