package main

import (
	"flag"

	"github.com/fsnotify/fsnotify"
	"github.com/gofiber/fiber"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"krungthai.com/example/ktbum/app/config"
	"krungthai.com/example/ktbum/app/db/postgres"
	"krungthai.com/example/ktbum/data/orm/users"
	mUser "krungthai.com/example/ktbum/model/users"
)

var (
	configPath = flag.String("config", "config_dev.toml", "Path of configuration file.")
	port       = flag.String("port", "8080", "Port for start server.")
	debug      = flag.Bool("debug", false, "sets log level to debug")
)

func init() {
	flag.Parse()
	config.LoadConfig(*configPath)
	postgres.InitConnection()
	viper.OnConfigChange(func(in fsnotify.Event) {
		postgres.InitConnection()
	})
	return
}
func main() {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	log.Info().Msg("Server start at port : " + *port)
	app := fiber.New()
	api := app.Group("/api")
	v1 := api.Group("/v1")
	userApi := v1.Group("/user")

	v1.Get("/probe", func(c *fiber.Ctx) {
		msg := "GET : Hi!! " + viper.GetString("application")
		c.Send(msg)
	})
	v1.Post("/probe", func(c *fiber.Ctx) {
		msg := "POST : Hi!! " + viper.GetString("application")
		c.Send(msg)
	})

	userApi.Get("/info", func(c *fiber.Ctx) {
		u := users.UserTest()
		c.JSON(u)
	})
	userApi.Post("/info", func(c *fiber.Ctx) {
		u := mUser.User{}
		c.BodyParser(&u)
		users.AddUserTest(&u)
		c.JSON(u)
	})

	log.Err(app.Listen(*port))
}
