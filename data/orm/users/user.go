package users

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/rs/zerolog/log"
	"krungthai.com/example/ktbum/app/db/postgres"
	"krungthai.com/example/ktbum/model/users"
)

func Test() {
	db, err := postgres.OpenConnection()
	defer db.Close()
	if err != nil {
		log.Info().Msg("Connect to Postgres Fail.")
	} else {
		log.Info().Msg("Connect to Postgres Success.")
	}
	u := users.User{}
	db.Table("USER").First(&u)
	log.Info().Msgf("Find : ", u)

}

func UserTest() *users.User {
	db, err := postgres.OpenConnection()
	defer db.Close()
	if err != nil {
		log.Error().Msg("Connect to Postgres Fail.")
	} else {
		log.Info().Msg("Connect to Postgres Success.")
	}
	u := users.User{}
	db.Table("USER").First(&u)
	return &u

}

func AddUserTest(u *users.User) {
	db, _ := postgres.OpenConnection()
	defer db.Close()
	db.Table("USER").Create(&u)
	// if !db.Table("USER").Update(&u) {
	// 	log.Error().Msg("Add to Postgres Fail.")
	// } else {
	// 	log.Info().Msg("Add to Postgres Success.")
	// }

}
