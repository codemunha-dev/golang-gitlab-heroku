module krungthai.com/example/ktbum

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber v1.13.3
	github.com/jinzhu/gorm v1.9.15
	github.com/klauspost/compress v1.10.10 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lib/pq v1.8.0 // indirect
	github.com/rs/zerolog v1.19.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/sys v0.0.0-20200810151505-1b9f1253b3ed // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
