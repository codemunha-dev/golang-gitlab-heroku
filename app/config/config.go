package config

import (
	"os"

	"github.com/rs/zerolog/log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

// LoadConfig is a read configuration from config file
func LoadConfig(fpath string) {
	viper.SetConfigFile(fpath)
	viper.WatchConfig()
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Msg("Error reading config file, " + err.Error())
	}

	host, _ := os.Hostname()
	viper.Set("hostname", host)
	log.Info().Msg("Load Configuration Completed." + viper.GetString("application"))
}
