package postgres

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

var connStr string

func InitConnection() {
	connStr = viper.GetString("db.postgres.connection")
	log.Info().Msg("Load Postgres Config Completed." + connStr)
}

func OpenConnection() (*gorm.DB, error) {
	log.Info().Msg("Connecting to Postgres...")
	return gorm.Open("postgres", connStr)
}
